const table = document.querySelector(".tab");
const all_inputs = document.querySelectorAll("#formulario input");
const button_input = document.querySelector(".input_button");
let allBooks;
let name_book = document.querySelector(".nameBook");
let name_author = document.querySelector(".nameAuthor");
let deleteButton = document.querySelector("table");



const my_header = {
    "Content-Type": "application/json"
}
const fetchConfig = {
    method: 'GET',
    headers: my_header
}


function get() {
    fetch ('https://treinamento-api.herokuapp.com/books', fetchConfig)
    .then((response)=>{
        return response.json();
    })
    .then((responseAsJson)=>{
        allBooks = responseAsJson;
    })
    .then(() =>{
 
        for(let i = 0; i < allBooks.length; i++){
            console.log(allBooks);
            let tabelaTr = document.createElement("tr");
            let tabelaId = document.createElement("td");
            let tabelaNome = document.createElement("td");
            let tabelaAutor = document.createElement("td");
            let tabelaCr = document.createElement("td");
            let tabelaUp = document.createElement("td");
            let button_delete = document.createElement("button");

            tabelaId.innerText = allBooks[i].id;
            tabelaNome.innerText = allBooks[i].name;
            tabelaAutor.innerText = allBooks[i].author;
            tabelaCr.innerText = allBooks[i].created_at;
            tabelaUp.innerText = allBooks[i].updated_at;
            button_delete.innerText = "Deletar";


            tabelaId.classList.add('id');
            button_delete.classList.add('delete');


            tabelaTr.appendChild(tabelaId);
            tabelaTr.appendChild(tabelaNome);
            tabelaTr.appendChild(tabelaAutor);
            tabelaTr.appendChild(tabelaCr);
            tabelaTr.appendChild(tabelaUp);
            tabelaTr.appendChild(button_delete);

            table.appendChild(tabelaTr);


        }

    }).catch(reject => console.log(`Erro, ${reject}`)); 
}

function post(name, author){
    const new_book = {
        "book":{
            "name":name,
            "author": author
        }
    }
    const my_header_post = {
        "Content-Type": "application/json"
    }
    const fetch_config_post = {
        method: 'POST',
        headers: my_header_post,
        body: JSON.stringify(new_book)
    }
    
    fetch ('https://treinamento-api.herokuapp.com/books', fetch_config_post)
    .then((response)=>{
        return response.json();
    })
    .then((response_as_json) => {
        let tabelaTr = document.createElement("tr");

        let tabelaId = document.createElement("td");
        let tabelaNome = document.createElement("td");
        let tabelaAutor = document.createElement("td");
        let tabelaCr = document.createElement("td");
        let tabelaUp = document.createElement("td");
        let button_delete = document.createElement("button");

        tabelaId.innerText = response_as_json.id;
        tabelaNome.innerText = response_as_json.name;
        tabelaAutor.innerText = response_as_json.author;
        tabelaCr.innerText = response_as_json.created_at;
        tabelaUp.innerText = response_as_json.updated_at;
        button_delete.innerText = "Deletar";

        
        tabelaId.classList.add('id');
        button_delete.classList.add('delete');

        tabelaTr.appendChild(tabelaId);
        tabelaTr.appendChild(tabelaNome);
        tabelaTr.appendChild(tabelaAutor);
        tabelaTr.appendChild(tabelaCr);
        tabelaTr.appendChild(tabelaUp);
        tabelaTr.appendChild(button_delete);

        table.appendChild(tabelaTr);

    }).catch(reject => console.log(`Erro, ${reject}`)); 
}

button_input.addEventListener('click', (e)=>{
    e.preventDefault();
    post(all_inputs[0].value, all_inputs[1].value);
})



function deleteBooks(id){
    fetch (`https://treinamento-api.herokuapp.com/books/${id}`,{
        method:'DELETE'
    })
}


deleteButton.addEventListener('click', (e) => {
    e.preventDefault();
    if (e.target.nodeName === 'BUTTON' && e.target.className === 'delete'){
        deleteBooks(e.target.parentNode.querySelector(".id").innerText);
        e.target.parentNode.remove();
    }
})


get ();
